import { Component, OnInit } from '@angular/core';
import { Produit } from 'src/app/models/produit';
import { ProduitService } from 'src/app/services/service-produit';
import { NavController, NavParams, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { EmprunteurService } from 'src/app/services/service-emprunteur';
import { Commente } from 'src/app/models/commente';
import { ProprietaireService } from 'src/app/services/service-proprietaire';
import { Proprietaire } from 'src/app/models/proprietaire';
import { Emprunteur } from 'src/app/models/emprunteur';
import { Emprunte } from 'src/app/models/emprunte';


@Component({
  selector: 'app-produits',
  templateUrl: './produits.component.html',
  styleUrls: ['./produits.component.scss'],
})
export class ProduitsComponent implements OnInit {

private produit : Produit;
private proprietaire : Proprietaire;
private emprunte : any;
private id : number;
private id2: number;
private datemprunt: Date;
private listeCommentaire : Commente[];
private emprunteur : Emprunteur;



  constructor(public produitService : ProduitService, public navCtrl: NavController, public navParams: NavParams, 
    route: ActivatedRoute, public proprietaireService : ProprietaireService, public emprunteurService : EmprunteurService) { 

    this.id = route.snapshot.params.id;
    this.id2 = route.snapshot.params.id2;

    this.produitService.getUnProduitParProprietaire(this.id, this.id2).subscribe(produit=>{
      this.produit = produit;
    });

    this.produitService.afficheCommentaire(this.id, this.id2).subscribe(listeCommentaire=>{
    this.listeCommentaire = listeCommentaire;

    console.log(listeCommentaire);

    });

    this.proprietaireService.getProprietaire(this.id).subscribe(proprietaire=>{
      this.proprietaire = proprietaire;
    })

    
   this.produitService.getDateEmprunt(this.id2).subscribe(datemprunt=>{

      this.datemprunt = datemprunt;

 
    });

  }


  ngOnInit() {
    this.emprunteurService.emprunteuracif(this.id, this.id2).subscribe(emprunteur =>{

      this.emprunteur = emprunteur;
  
     
  
  
      })
  

  }

}
