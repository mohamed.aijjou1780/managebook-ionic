import { Component, OnInit } from '@angular/core';
import { ProprietaireService } from 'src/app/services/service-proprietaire';
import { NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-param-principal',
  templateUrl: './param-principal.component.html',
  styleUrls: ['./param-principal.component.scss'],
})
export class ParamPrincipalComponent implements OnInit {

prop : boolean;
id:number;

  constructor(public proprietaireService : ProprietaireService, public navCtrl : NavController, public router : Router, public route: ActivatedRoute ) { 
    this.id = route.snapshot.params.id;
  }

  ngOnInit() {}



paramProp(){
this.prop = true;
this.proprietaireService.isProprietaire(this.prop);
this.navCtrl.navigateForward('proprietaire/' + this.id + '/paramp/params');
 }

paramEmprun(){
this.prop = false;
console.log(this.prop);
this.proprietaireService.isProprietaire(this.prop);
this.navCtrl.navigateForward('proprietaire/' + this.id + '/paramp/params');
}

}
