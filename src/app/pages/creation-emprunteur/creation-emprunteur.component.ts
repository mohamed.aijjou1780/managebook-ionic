import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EmprunteurService } from 'src/app/services/service-emprunteur';
import { ActivatedRoute } from '@angular/router';
import { ToastController, NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-creation-emprunteur',
  templateUrl: './creation-emprunteur.component.html',
  styleUrls: ['./creation-emprunteur.component.scss'],
})
export class CreationEmprunteurComponent implements OnInit {



  id: number;
  onForm = new FormGroup({
    nom : new FormControl(),
    prenom : new FormControl(),
    lien : new FormControl(),

  });

  async presentToast() {
    const toast = await this.toastController.create({
      color: 'success',
      message: 'Emprunteur crée',
      duration: 800
    });
    toast.present();
   await   this.navCtrl.navigateForward('proprietaire/' + this.id); 
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Echec Creation',
      message: 'L Emprunteur existe déjà',
      buttons: [
        {
          text: 'OK',
          role: 'OK',
          cssClass: 'danger',
          handler: () =>{
          this.navCtrl.navigateBack( '/proprietaire/' + this.id + '/creationemprunteur');
        }
      }
      ], 
    });

    await alert.present();
  }


  constructor(@Inject(FormBuilder) public formBuilder: FormBuilder, public emprunteurService : EmprunteurService,
   public route : ActivatedRoute, public toastController: ToastController, public navCtrl: NavController, 
   public alertController: AlertController) {

  this.id = route.snapshot.params.id;

    this.onForm = this.formBuilder.group({
      
      nom: ['', Validators.required],
      prenom : ['', Validators.required],
      lien : ['', Validators.required],
    });
   }

   public creation(): void {
    this.emprunteurService.creationEmprunteur(this.id, this.onForm.value).subscribe(res=>{
      console.log(res.message);
      if(res.message === 'L Emprunteur a été crée'){  
      this.presentToast();

      }
      if(res.message === 'L emprunteur existe déjà'){
    this.presentAlert();
      } 
    }
    );
  }

  ngOnInit() {}

}
