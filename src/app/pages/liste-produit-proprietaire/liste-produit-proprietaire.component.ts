import { Component, OnInit, ViewChild } from '@angular/core';
import { ProduitService } from 'src/app/services/service-produit';
import { NavParams, NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Produit } from 'src/app/models/produit';
import { Location } from "@angular/common";
import { Proprietaire } from 'src/app/models/proprietaire';
import { ProprietaireService } from 'src/app/services/service-proprietaire';
import { GoogleChartInterface } from 'ng2-google-charts';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-liste-produit-proprietaire',
  templateUrl: './liste-produit-proprietaire.component.html',
  styleUrls: ['./liste-produit-proprietaire.component.scss'],
})
export class ListeProduitProprietaireComponent implements OnInit {
  @ViewChild('barChart', {static: false}) barChart;


  private listeproduit: Produit[];
  private produit: Produit;
  id: number;
  i: number;

  private proprietaire: Proprietaire;


  listDvd = new Set();
  listLivre = new Map();
  listCd = new Map();
  listJeux = new Map();
  listPret = new Map();
  qteLivre: number;
  qteJeux: number;
  qteDvd: number;
  qteCd: number;
  qtePret: number;
  listecomplete = new Map();
  listecouleur = [];
  pie : any;
  totalliste : number;
  tauxpret : number;

  




  constructor(public router: Router, public produitService: ProduitService, public params: NavParams,
    public route: ActivatedRoute, private location: Location, private navCrtl: NavController, public proprietaireService: ProprietaireService) {

  }

  ionViewDidEnter() {

    

    this.id = this.route.snapshot.params.id;

    this.proprietaireService.getProprietaire(this.id).subscribe(proprietaire => {
      this.proprietaire = proprietaire;
    });
    this.produitService.getListProduitsParProprietaire(this.id).subscribe(listeproduit => {
      this.listeproduit = listeproduit;
      this.listeproduit.forEach(produit => {
        this.produit = produit;

        if (this.produit.type === 'Dvd') {
          this.listDvd.add(produit.type);
        }
        if (this.produit.type === 'Livre') {
      
          this.listLivre.set(produit.id, produit.type);
        }
        if (this.produit.type === 'Cd') {
          this.listCd.set(produit.id, produit.type);
        }
        if (this.produit.type === 'Jeux') {
          this.listJeux.set(produit.id, produit.type);
        }
        if (this.produit.present === 'false') {
          this.listPret.set(produit.id, produit.present);

        }

      });
      this.qteLivre = this.listLivre.size;
      this.qteJeux = this.listJeux.size
      this.qteDvd = this.listDvd.size;
      this.qteCd = this.listCd.size;
      this.qtePret = this.listPret.size;
      this.totalliste = this.listeproduit.length;

      this.tauxpret = ((this.qtePret / this.totalliste)*100);

      console.log(this.qteLivre);
      console.log(this.qteJeux);
      console.log(this.qteDvd);
      console.log(this.qteCd);
      console.log(this.qtePret);

      this.listecouleur.push('#f7bf86');
      this.listecouleur.push('#f786be');
      this.listecouleur.push('#d4a3f6');
      this.listecouleur.push('#86f6f7');

     

      this.pie = new Chart(this.barChart.nativeElement, {
        type: 'pie',
        data: {
          labels: ['livre', 'dvd', 'cd', 'jeux'],
          datasets: [{
            label: 'Quantité objet',
            data: [this.qteLivre,this.qteDvd,this.qteCd,this.qteJeux],
            backgroundColor: this.listecouleur,
            borderColor: '#514845',
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });

      

    });

    
  
  

    

  }


 

  myBackButton() {
    console.log('go go')
    this.router.navigate(['/proprietaire/', this.id]);
  }

  ngOnInit() {


  }
}