import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, NgForm, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ProprietaireService } from 'src/app/services/service-proprietaire';
import { Proprietaire } from 'src/app/models/proprietaire';
import { NavController, LoadingController, ToastController, AlertController } from '@ionic/angular';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  id: number;
  proprietaire: Proprietaire;
  identifiant: string;
  res: any;


  loginForm = new FormGroup({
    identifiant: new FormControl(),
    mdp: new FormControl(),
    isLoadingResults: new FormControl(),
  });



  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Echec Identification',
      message: 'Mot de passe ou Identifiant incorrects',
      buttons: [
        {
          text: 'OK',
          role: 'OK',
          cssClass: 'danger',
          handler: () =>{

          this.navCtrl.navigateBack('login');
        }
      }
      ], 
    });

    await alert.present();
  }


  async presentToast() {
    const toast = await this.toastController.create({
      color: 'success',
      message: 'login successfully',
      duration: 800
    });
    toast.present();
    await this.presentLoading();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 1200
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
    
  }

  constructor(public alertController: AlertController, public toastController: ToastController, 
    public loadingController: LoadingController, public navCtrl: NavController, private formBuilder: FormBuilder,
     public router: Router, private authService: AuthService, public proprietaireService: ProprietaireService) {

  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      identifiant: [null, Validators.required],
      mdp: [null, Validators.required]
    });

  }

  public login() {
    this.proprietaireService.getIdProprietaireByIdentifiant(this.loginForm.value.identifiant).subscribe(id => {
      this.id = id;
    })
    this.authService.login(this.loginForm.value)
      .subscribe(res => {
        console.log(res.message);
       const status = localStorage.getItem('status');
        console.log(status);
       if (res.message === 'login successfully' && this.authService.isLoggedIn && status === '200') { 
            console.log('c est ok');
        this.presentToast();
         this.navCtrl.navigateForward('proprietaire/' + this.id); 
         
       }
        else {
          this.presentAlert();
          console.log('erreur mon gars');
        }

      }, (err) => {
        console.log(err);
      });


  }



  register() {
    this.router.navigate(['register']);
  }
  
}
