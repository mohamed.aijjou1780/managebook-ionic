import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProduitService } from 'src/app/services/service-produit';
import { ProprietaireService } from 'src/app/services/service-proprietaire';
import { Proprietaire } from 'src/app/models/proprietaire';
import { Produit } from 'src/app/models/produit';
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-commentaire',
  templateUrl: './commentaire.component.html',
  styleUrls: ['./commentaire.component.scss'],
})
export class CommentaireComponent implements OnInit {

  produit : Produit;
  proprietaire : Proprietaire;
  id : number;
  id2 : number;
  onFormCommente = new FormGroup({
    dateCommente : new FormControl,
    note : new FormControl,
    commentaire : new FormControl,
    produit : new FormControl,
    emprunteur : new FormControl,
    proprietaire : new FormControl,

  });

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Création',
      subHeader: 'Commentaire et Note',
      message: 'Opération effectuée',
      buttons: [
        {
          text: 'OK',
          role: 'OK',
          cssClass: 'secondary',
          handler: () =>{

          this.navCtrl.navigateBack('proprietaire/' + this.id + '/produits');
  
        }

      }
      ], 
     
    });

    await alert.present();
  }



  constructor(route: ActivatedRoute, @Inject(FormBuilder) public formBuilder: FormBuilder, 
  public produitService : ProduitService, public proprietaireService : ProprietaireService, 
  public alertController : AlertController, public navCtrl : NavController ) { 

    this.id = route.snapshot.params.id;
    this.id2 = route.snapshot.params.id2;

    this.produitService.getUnProduitParProprietaire(this.id, this.id2).subscribe(produit=>{
      this.produit = produit;


  });

  this.onFormCommente = this.formBuilder.group({
    note : ['', Validators.required],
    commentaire : ['', Validators.required],
  });
  
  }
  creationCommentaire(){
this.produitService.commenteProduit(this.id, this.id2, this.onFormCommente.value.note, this.onFormCommente.value.commentaire).subscribe();

console.log(this.onFormCommente.value.note);

console.log(this.onFormCommente.value.commentaire);

this.presentAlert();

  }
 

  ngOnInit() {}

}
