import { Component, OnInit } from '@angular/core';
import { Proprietaire } from 'src/app/models/proprietaire';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

  proprietaire: Proprietaire;

  onForm = new FormGroup({
    nom: new FormControl(),
    prenom: new FormControl(),
    avatar: new FormControl(),
    description: new FormControl(),
    identifiant: new FormControl(),
    mdp: new FormControl(),
  })

  constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthService) { }
  ngOnInit() {

    this.onForm = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      avatar: ['', Validators.required],
      description: ['', Validators.required],
      identifiant: ['', Validators.required],
      mdp: ['', Validators.required],

    });
  }
  public creation(): void {
    console.log(this.onForm.value);
    this.authService.register(this.onForm.value).subscribe(res => {
      console.log(res);
      this.router.navigate(['login']);
    }, (err) => {
      console.log(err);
      alert(err.error);
    });;
  }
}
