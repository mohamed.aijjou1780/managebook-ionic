import { Component, OnInit } from '@angular/core';
import { Proprietaire } from 'src/app/models/proprietaire';
import { ProduitService } from 'src/app/services/service-produit';
import { ProprietaireService } from 'src/app/services/service-proprietaire';
import { NavParams } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Produit } from 'src/app/models/produit';

@Component({
  selector: 'app-proprietaire-unique',
  templateUrl: './proprietaire-unique.component.html',
  styleUrls: ['./proprietaire-unique.component.scss'],
})
export class ProprietaireUniqueComponent implements OnInit {

  private proprietaire: Proprietaire;
  private id: number;
  private listeproduit: Produit[];
  private produit: Produit;
  listDvd = new Set();
  listLivre = new Map();
  listCd = new Map();
  listJeux = new Map();
  listPret = new Map();
  qteLivre: number;
  qteJeux: number;
  qteDvd: number;
  qteCd: number;
  qtePret: number;


  constructor(public proprietaireService: ProprietaireService, public params: NavParams,
    public route: ActivatedRoute, private authservice: AuthService, private router: Router, public produitService: ProduitService) {
  

    }

  ionViewDidEnter() {
    this.id = this.route.snapshot.params.id;

    this.proprietaireService.getProprietaire(this.id).subscribe(proprietaire => {
      this.proprietaire = proprietaire;
    });

    this.produitService.getListProduitsParProprietaire(this.id).subscribe(listeproduit => {
      this.listeproduit = listeproduit;
    });

  }





  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }

  ngOnInit() {


}
}