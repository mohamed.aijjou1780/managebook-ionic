import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ProduitService } from 'src/app/services/service-produit';
import { ProprietaireService } from 'src/app/services/service-proprietaire';
import { Proprietaire } from 'src/app/models/proprietaire';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-creation-produit',
  templateUrl: './creation-produit.component.html',
  styleUrls: ['./creation-produit.component.scss'],
})
export class CreationProduitComponent implements OnInit {

  formgroup : FormGroup;
  public id : number;
  public idProprietaire : number;
  public proprietaire : Proprietaire;
  public route: ActivatedRoute;
  onForm = new FormGroup({
    titre : new FormControl(),
    auteur : new FormControl(),
    type : new FormControl(),
    image : new FormControl(),
    image2 : new FormControl(),
    edition : new FormControl(),
    ean : new FormControl(),
    description : new FormControl(),
    date : new FormControl(),
    present : new FormControl(),
  });

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Validation',
      subHeader: 'Creation Objet',
      message: 'Opération effectuée',
      buttons: [
        {
          text: 'OK',
          role: 'OK',
          cssClass: 'secondary',
          handler: () =>{

          this.navCtrl.navigateBack('proprietaire/' + this.id );
          /* this.router.navigate(['/proprietaire/', this.id]); */
  
        }

      }
      ], 
     
    });

    await alert.present();
  }

  constructor(public router : Router, public navCtrl : NavController, 
    public alertController : AlertController, route: ActivatedRoute, @Inject(FormBuilder) public formBuilder: FormBuilder, 
    public produitService : ProduitService, public proprietaireService : ProprietaireService ) { 

    this.id = route.snapshot.params.id;

    

    this.onForm = this.formBuilder.group({

      titre: ['', Validators.required],
      auteur : ['', Validators.required],
      type : ['', Validators.required],
      image : ['', Validators.required],
      image2 : ['', Validators.required],
      edition : ['', Validators.required],
      ean : ['', Validators.required],
      description : ['', Validators.required],
      present: ['', Validators.required],
      date : ['', Validators.required],
      
    });

    this.proprietaireService.getProprietaire(this.id).subscribe(proprietaire => {
      this.proprietaire = proprietaire;

      
    });

  }

  public creationProduit(): void{

    console.log(this.onForm.value.titre);
    console.log(this.id);
    console.log( this.onForm.value.date);
    
    this.produitService.creationProduit(this.id, this.onForm.value).subscribe();
    this.presentAlert();
      
      }


  ngOnInit() {
    
  }

}
