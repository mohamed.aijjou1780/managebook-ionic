import { Component, OnInit } from '@angular/core';
import { Produit } from 'src/app/models/produit';
import { ProduitService } from 'src/app/services/service-produit';
import { NavParams, ActionSheetController, NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
})
export class SearchPageComponent implements OnInit {

  private listeproduit: Produit[];
  public loadedGoalList: Produit[];
  message2: string;
  res: any;

  id: number;

  constructor(public produitService: ProduitService, public params: NavParams, route: ActivatedRoute,
    public actionSheetController: ActionSheetController, public navCtrl: NavController, public router: Router,
    public toastController: ToastController, public navControl: NavController) {

    this.id = route.snapshot.params.id;
  }

  async presentToast() {
    const toast = await this.toastController.create({
      color: 'success',
      duration: 3000,
      message: 'Objet Supprimé',
      position: 'middle',
      buttons: [
        {
          text: 'OK',
          handler: () => {

            this.navControl.navigateBack('proprietaire/' + this.id);

          }
        }
      ]
    });
    toast.present();
  }

  async presentAlert() {
    const alert = await this.toastController.create({
      color: 'danger',
      header: 'Attention',
      message: 'Objet pas supprimé',
      duration: 3000,
    });

    await alert.present();
  }

  async presentActionSheet(idProduit) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Collection',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.produitService.supprimeUnProduit(this.id, idProduit).subscribe(res => {
            this.res = res;
            const mes = this.res.message;
            console.log(mes);
            if (mes === 'Objet supprimé') {
              this.presentToast();

            } else {
              this.presentAlert();
            }
          }
          );
        }
      }, {
        text: 'Open',
        icon: 'arrow-forward',
        handler: () => {
          this.navCtrl.navigateForward('proprietaire/' + this.id + '/produits/' + idProduit);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }



  ngOnInit() {

    this.produitService.getListProduitsParProprietaire(this.id).subscribe(listeproduit => {

      this.listeproduit = listeproduit;
      this.loadedGoalList = listeproduit;

    });

  }

  initializeItems(): void {

    this.listeproduit = this.loadedGoalList;

  }

  handleInput(ev) {

    this.initializeItems();

    const searchTerm = ev.srcElement.value;

    if (!searchTerm) {
      return;
    }
    this.listeproduit = this.listeproduit.filter(produit => {
      if (produit.titre && produit.auteur && searchTerm) {
        if (produit.titre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  handleInputSearch(event) {

    this.initializeItems();

    const searchTermAuteur = event.srcElement.value;

    if (!searchTermAuteur) {
      return;
    }
    this.listeproduit = this.listeproduit.filter(produit => {
      if (produit.auteur && searchTermAuteur) {
        if (produit.auteur.toLowerCase().indexOf(searchTermAuteur.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });

  }


  selectProduct(idProduit) {
    this.presentActionSheet(idProduit);
  }

  myBackButton() {
    this.router.navigate(['/proprietaire/', this.id]);
  }

}
