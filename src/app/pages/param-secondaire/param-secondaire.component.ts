import { Component, OnInit } from '@angular/core';
import { ProprietaireService } from 'src/app/services/service-proprietaire';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-param-secondaire',
  templateUrl: './param-secondaire.component.html',
  styleUrls: ['./param-secondaire.component.scss'],
})
export class ParamSecondaireComponent implements OnInit {

  isProp : boolean;
id: number;
  constructor(public proprietaireService: ProprietaireService, public navCtrl : NavController, public route: ActivatedRoute ) { 
    this.id = route.snapshot.params.id;
  }

  ngOnInit() { }

  ionViewDidEnter() {

    if (this.proprietaireService.isProp == true) {
      return this.isProp = true;
    } else {
      return this.isProp = false;
    }

  }

  navigeCreationEmprunteur(){
    this.navCtrl.navigateForward('proprietaire/' + this.id + '/creationemprunteur');
  }
}
