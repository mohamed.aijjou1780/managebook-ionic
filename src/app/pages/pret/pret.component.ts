import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { NavParams, ModalController, ToastController, NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Emprunteur } from 'src/app/models/emprunteur';
import { ProduitService } from 'src/app/services/service-produit';
import { Produit } from 'src/app/models/produit';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Emprunte } from 'src/app/models/emprunte';
import { EmprunteurService } from 'src/app/services/service-emprunteur';






@Component({
  selector: 'app-pret',
  templateUrl: './pret.component.html',
  styleUrls: ['./pret.component.scss'],
})
export class PretComponent implements OnInit {


  idProduit : number;
  private listeEmprunteur: Emprunteur[];
  private id: number;
  private listeProduitDispo: Produit[];
  private listeProduitNonDispo: Produit[];
  private emprunte: Emprunte;
  private emprunteurActif : Emprunteur;
  private toast : any;
  private data : any;
  private vidEmprunt : boolean;
  private vidRetour : boolean;

private datemprunt : Date;

  onForm = new FormGroup({
    idEmprunteur: new FormControl(),
    idProduit: new FormControl(),
  
  });

  onFormRetour = new FormGroup({
    idEmprunteur2: new FormControl(),
    idProduit2: new FormControl(),
  });

  async presentToast() {
    const toast = await this.toastController.create({
      color: 'success',
      duration: 3000,
      message: 'Prêt validé',
      position: 'middle',
      buttons: [
        {
          text: 'OK',
          handler: () => {
       
         /*    this.navControl.navigateBack('proprietaire/' + this.id); */
         
         this.router.navigate(['/proprietaire/', this.id]);
  
          }
        }
      ]
    });
    toast.present();

  }

  async presentAlert() {
    const alert = await this.toastController.create({
      color:'danger',
      header: 'Attention',
      message: 'La validation n a pas fonctionné',
      duration: 3000,
    });

    await alert.present();
  }

  async presentRetour() {
  this.toast = this.toastController.create({
        
    color: 'tertiary',
    duration: 2000,
    message: 'Retour validé',
    position: 'middle',
    buttons: [
      {
        text: 'OK',
        handler: () => {
       /*    this.router.navigate(['/proprietaire/', this.id]); */
          this.navControl.navigateRoot('proprietaire/' + this.id);

        }
      }
    ]

  }).then((toastData)=>{
    console.log(toastData);
    toastData.present();

  });
  }
  constructor(@Inject(ProduitService) public produitService: ProduitService, 
  @Inject(NavParams) public params: NavParams, @Inject(ActivatedRoute) public route: ActivatedRoute, 
  @Inject(FormBuilder) public formBuilder: FormBuilder, public modalController : ModalController, 
  public emprunteurService : EmprunteurService, public formBuilder2: FormBuilder, public toastController: ToastController, 
  public navControl : NavController, public router: Router) {


    this.id = route.snapshot.params.id;

    this.produitService.getEmprunteur(this.id).subscribe(listeEmprunteur => {
      this.listeEmprunteur = listeEmprunteur;
    })



    this.produitService.getListProduitsDispo(this.id).subscribe(listeProduitDispo => {
      this.listeProduitDispo = listeProduitDispo;

      this.listeProduitDispo.length

      if(this.listeProduitDispo.length != 0){

        this.vidEmprunt = true;

      }if(this.listeProduitDispo.length == 0){

        this.vidEmprunt = false;
      }
       return this.vidEmprunt;

    })
  
    this.produitService.getListProduitsNonDispo(this.id).subscribe(listeProduitNonDispo=> {
    this.listeProduitNonDispo = listeProduitNonDispo;

    this.listeProduitNonDispo.length

    if(this.listeProduitNonDispo.length != 0){

      this.vidRetour = true;

    }if(this.listeProduitNonDispo.length == 0){

      this.vidRetour = false;
    }
    return this.vidRetour;

     })

     this.onForm = this.formBuilder.group({
      idEmprunteur: ['', Validators.required],
      idProduit: ['', Validators.required],
      
    });

    this.onFormRetour = this.formBuilder2.group({
      idEmprunteur2: ['', Validators.required],
      idProduit2: ['', Validators.required],
    })
  }
  public emprunter(): void {
  
    const id2 = this.onForm.value.idEmprunteur;
    const id3 = this.onForm.value.idProduit;
   
    this.produitService.emprunte(id2, id3).subscribe(_=>{

      if(this.onForm.value != null ){
        this.presentToast();
        this.produitService.getListProduitsNonDispo(this.id).subscribe(listeProduitNonDispo=> {
          this.listeProduitNonDispo = listeProduitNonDispo;
      
          this.listeProduitNonDispo.length
      
          if(this.listeProduitNonDispo.length != 0){
      
            this.vidRetour = true;
      
          }if(this.listeProduitNonDispo.length == 0){
      
            this.vidRetour = false;
          }
          return this.vidRetour;
      
           })

           this.produitService.getListProduitsDispo(this.id).subscribe(listeProduitDispo => {
            this.listeProduitDispo = listeProduitDispo;
      
            this.listeProduitDispo.length
      
            if(this.listeProduitDispo.length != 0){
      
              this.vidEmprunt = true;
      
            }if(this.listeProduitDispo.length == 0){
      
              this.vidEmprunt = false;
            }
             return this.vidEmprunt;
      
          })

      }
      if(this.onForm.value == null){

        this.presentAlert();
      }

    });

  }


  public selectObjet(event){

    console.log(event.detail.value);
    this.emprunteurService.emprunteuracif(this.id, event.detail.value).subscribe(emprunteurActif => {
      this.emprunteurActif = emprunteurActif;

    });

  }


  public retour(): void{

    const id2 = this.onFormRetour.value.idEmprunteur2;
    const id3 = this.onFormRetour.value.idProduit2;

    console.log(id2, id3);
    this.produitService.retour(id2, id3).subscribe(
      _=>{

        if(this.onFormRetour.value != null ){
          this.presentRetour();
          this.produitService.getListProduitsNonDispo(this.id).subscribe(listeProduitNonDispo=> {
            this.listeProduitNonDispo = listeProduitNonDispo;
        
            this.listeProduitNonDispo.length
        
            if(this.listeProduitNonDispo.length != 0){
        
              this.vidRetour = true;
        
            }if(this.listeProduitNonDispo.length == 0){
        
              this.vidRetour = false;
            }
            return this.vidRetour;
        
             })

             this.produitService.getListProduitsDispo(this.id).subscribe(listeProduitDispo => {
              this.listeProduitDispo = listeProduitDispo;
        
              this.listeProduitDispo.length
        
              if(this.listeProduitDispo.length != 0){
        
                this.vidEmprunt = true;
        
              }if(this.listeProduitDispo.length == 0){
        
                this.vidEmprunt = false;
              }
               return this.vidEmprunt;
        
            })
         
        }else{

          this.presentAlert();
        }
  
      });

  }
  

 

  
    


  ngOnInit() {
   
      
  }
   

}
