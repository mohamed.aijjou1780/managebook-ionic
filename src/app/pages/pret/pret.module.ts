import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { PretComponent } from './pret.component';



@NgModule({
    imports: [
      CommonModule,
      IonicModule,
      ReactiveFormsModule,
      RouterModule.forChild([
        {
          path: '',
          component: PretComponent
        }
      ])
    ],
    declarations: [PretComponent]
  })

export class PretComponentModule{




}