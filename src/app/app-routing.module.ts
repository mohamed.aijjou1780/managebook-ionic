import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { ProduitsComponent } from './pages/produits/produits.component';
import { ListeProduitProprietaireComponent } from './pages/liste-produit-proprietaire/liste-produit-proprietaire.component';
import { ProprietaireUniqueComponent } from './pages/proprietaire-unique/proprietaire-unique.component';
import { PretComponent } from './pages/pret/pret.component';
import { CreationProduitComponent } from './pages/creation-produit/creation-produit.component';
import { CommentaireComponent } from './pages/commentaire/commentaire.component';
import { SearchPageComponent } from './pages/search-page/search-page.component';
import { SearchBarcodeComponent } from './pages/search-barcode/search-barcode.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { AuthGuard } from './auth/auth.guard';
import { ParamPrincipalComponent } from './pages/param-principal/param-principal.component';
import { ParamSecondaireComponent } from './pages/param-secondaire/param-secondaire.component';
import { CreationEmprunteurComponent } from './pages/creation-emprunteur/creation-emprunteur.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
      { path: 'proprietaire/:id/produits/:id2',canActivate: [AuthGuard], component: ProduitsComponent },
      { path: 'proprietaire/:id',canActivate: [AuthGuard],component: ProprietaireUniqueComponent },
      { path: 'proprietaire/:id/produits',canActivate: [AuthGuard], component: ListeProduitProprietaireComponent },
      { path: 'proprietaire/:id/emprunteur',canActivate: [AuthGuard], component:  PretComponent },
      { path: 'proprietaire/:id/produit',canActivate: [AuthGuard],component:  CreationProduitComponent},
      { path: 'proprietaire/:id/produits/:id2/commentaire',canActivate: [AuthGuard], component:  CommentaireComponent },
      { path: 'proprietaire/:id/search',canActivate: [AuthGuard], component:  SearchPageComponent},
      { path: 'proprietaire/:id/search/barcode',canActivate: [AuthGuard], component:  SearchBarcodeComponent },
      { path: 'proprietaire/:id/paramp',canActivate: [AuthGuard], component:  ParamPrincipalComponent },
      { path: 'proprietaire/:id/paramp/params',canActivate: [AuthGuard], component:  ParamSecondaireComponent },
      { path: 'proprietaire/:id/creationemprunteur',canActivate: [AuthGuard], component:  CreationEmprunteurComponent },
      { path: 'login', component:  LoginComponent },
      { path: 'register', component:  RegisterComponent },
      { path: '**', component:   PretComponent },

  
      
       
  ]), 
],
  exports: [RouterModule]
})
export class AppRoutingModule { }
