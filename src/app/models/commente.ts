import { Produit } from './produit';
import { EmprunteurService } from '../services/service-emprunteur';
import { Emprunteur } from './emprunteur';
import { Proprietaire } from './proprietaire';

export class Commente{
    dateCommnte : Date;
    note : number;
    commentaire : string;
    produit : Produit;
    emprunteur : Emprunteur;
    proprietaire : Proprietaire;
    
    
    Constructor(
        dateCommnte : Date,
        note : number,
        commentaire : string,
        produit : Produit,
        emprunteur : Emprunteur,
        proprietaire : Proprietaire,
        )
        {

            this.dateCommnte = dateCommnte;
            this.note = note;
           this.commentaire = commentaire;  
           this.produit = produit;
           this.emprunteur = emprunteur;
           this.proprietaire = proprietaire;  
    }


    
    
    
    }