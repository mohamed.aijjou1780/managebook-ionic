
import { Proprietaire } from './proprietaire';

export class Produit{
id: number;
titre : string;
auteur : string;
type : string;
image : string;
image2 : string;
ean : string;
edition: string;
description : string;
date : Date;
present : string;
proprietaire : Proprietaire;
listemprunt : string[];



Constructor(
    id: number,
    titre : string,
    auteur : string,
    type : string,
    image : string,
    image2 : string,
    ean : string,
    edition: string,
    description : string,
    date : Date,
    present : string,
    proprietaire : Proprietaire,
    listemprunt : string[]){

        this.id = id;
        this.auteur = auteur;
        this.titre = titre;
        this.type = type;
        this.date = date;
        this.description = description;
        this.present = present;
        this.ean = ean;
        this.edition = edition;
        this.image = image;
        this.image2 = image2;
        this.listemprunt = listemprunt;
        this.proprietaire = proprietaire;

}


}