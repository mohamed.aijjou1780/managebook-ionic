
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { ProprietaireService } from '../services/service-proprietaire';
import { NavController } from '@ionic/angular';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  id: number;
  stat : string;

  constructor(private router: Router, public authService: AuthService, public proprietaireService: ProprietaireService, public navCtrl: NavController) { }


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = localStorage.getItem('token');
    console.log(token);


    if (token) {
      request = request.clone({
        setHeaders: {
          'Authorization': 'Bearer ' + token
        }
      });
    }
    if (!request.headers.has('Content-Type')) {
      request = request.clone({
        setHeaders: {
          'content-type': 'application/json'
        }
      });
    }
    request = request.clone({
      headers: request.headers.set('Accept', 'application/json')
    });
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log('event--->>>', event);

          if(event.body.message != null){
            this.stat = JSON.stringify(event.status);
           console.log(event.body.message);
          localStorage.setItem('message', event.body.message); 
          localStorage.setItem('status',  this.stat); 
        
          console.log(this.stat);
          }else{
            console.log('le message est null');
          }

          /*  this.router.navigate(['register']); */
          /* this.proprietaireService.getIdProprietaireByIdentifiant(ident).subscribe(id =>{
            this.id = id;
            this.navCtrl.navigateForward('proprietaire/' + this.id ); 
          }) */
        /*   console.log(event.body.id);
          var event1 = event.body.id; */
         /*  let event2 = event1.toString; */
         /*  localStorage.setItem('status', event.status);   */
         /*  this.navCtrl.navigateForward('proprietaire/' + event1 );   */
        }
        return event;

      }),
      catchError((error: HttpErrorResponse) => {
        console.log(error);
        if (error.status === 401) {
          this.router.navigate(['login']);
        }
        if (error.status === 400) {
          alert(error.error);
        }
        return throwError(error);
      }));
  }

 


}