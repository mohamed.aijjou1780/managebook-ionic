import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Produit } from '../models/produit';
import { Emprunte } from '../models/emprunte';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { Commente } from '../models/commente';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class ProduitService {

    public API = 'http://localhost:8080';

    public msg: string = 'ça marche';
    public msg2: string = 'ça marche pas';


    constructor(public http: HttpClient, public loadingCtrl: LoadingController, public toastControl: ToastController) {
    }




    getProduits(): Observable<any> {
        return this.http.get(this.API + '/produits');

    }

    getListProduitsParProprietaire(id: number): Observable<any[]> {
        return this.http.get<any[]>(this.API + '/proprietaire/' + id + '/produits');
    }

    getUnProduitParProprietaire(id: number, id2: number): Observable<any> {
        return this.http.get<any>(this.API + '/proprietaire/' + id + '/produits/' + id2)

    }

    getListProduitsDispo(id: number): Observable<any[]> {
        return this.http.get<any[]>(this.API + '/proprietaire/' + id + '/produitsdispo');

    }

    getListProduitsNonDispo(id: number): Observable<any[]> {
        return this.http.get<any[]>(this.API + '/proprietaire/' + id + '/produitsnondispo');

    }

    getEmprunteur(id: number): Observable<any[]> {
        return this.http.get<any[]>(this.API + '/proprietaire/' + id + '/emprunteur');

    }

    emprunte(id2: number, id3: number): Observable<any> {
        return this.http.get<any>(this.API + '/emprunte/' + id2 + '/' + id3);

    }

    getEmprunt(): Observable<Emprunte[]> {
        return this.http.get<Emprunte[]>(this.API + '/empruntes');

    }
    creationProduit(id: number, produit: Produit): Observable<Produit> {
        return this.http.post<Produit>(this.API + '/proprietaire/' + id + '/produit', produit);


    }

    retour(id2: number, id3: number): Observable<any[]> {

        return this.http.get<any[]>(this.API + '/retour/' + id2 + '/' + id3);

    }

    commenteProduit(id: number, id2: number, note: number, commentaire: string): Observable<any> {
        return this.http.get<any>(this.API + '/proprietaire/' + id + '/produits/' + id2 + '/commente?note=' + note + '&commentaire=' + commentaire);

    }

    afficheCommentaire(id: number, id2: number): Observable<Commente[]> {

        return this.http.get<Commente[]>(this.API + '/proprietaire/' + id + '/produits/' + id2 + '/listecommentaire');
    }


    getDateEmprunt(idProduit: number): Observable<Date> {

        return this.http.get<Date>(this.API + '/empruntedate/' + idProduit);
    }

    supprimeUnProduit(id: number, id2: number) {

        return this.http.delete(this.API + '/proprietaire/' + id + '/search/' + id2 + '/delete')
        
    }


}