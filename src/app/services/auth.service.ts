import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { tap, catchError } from 'rxjs/operators';
import { Proprietaire } from '../models/proprietaire';
import { Observable, of } from 'rxjs';
import { ProprietaireService } from './service-proprietaire';
import { NavController } from '@ionic/angular';

@Injectable()
export class AuthService{

    public API = 'http://localhost:8080';

    isLoggedIn = false;
    redirectUrl: string;
    public proprietaire : Proprietaire;
    public data : any;
    public id : number;

    constructor(private http: HttpClient) { }



    islogin(){
      if (localStorage.getItem('message') === 'login successfully'){
        return this.isLoggedIn = true;
        console.log('vrai');
      }else {
       console.log('faux');
        return this.isLoggedIn = false;
      }
    }


    login(data: any): Observable<any> {
      console.log(data);
        return this.http.post<any>(this.API + '/login', data)
        .pipe(
            tap(_=>this.isLoggedIn = true ),
            catchError(this.handleError('login', []))
            );
        
         return data;
      }
      
    /*   logout(): Observable<any> {
        return this.http.get<any>(this.API + '/login')
        .pipe(
            tap(_=>this.isLoggedIn = false ),
            catchError(this.handleError('login', []))
      );
      
          
      } */
      
      register(proprietaire : Proprietaire): Observable<Proprietaire> {
        return this.http.post<Proprietaire>(this.API + '/register', proprietaire)
        .pipe(
            tap(_=>this.isLoggedIn = false ),
        );
         
      }
      
      private log(message: string) {
        console.log(message);
      }


      private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
      
          console.error(error); // log to console instead
          this.log(`${operation} failed: ${error.message}`);
      
          return of(result as T);
        };
      }
      
        
 
      




}