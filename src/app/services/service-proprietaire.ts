import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Proprietaire } from '../models/proprietaire';




@Injectable()
export class ProprietaireService{

    public proprietaire : Proprietaire;
    isProp:boolean;

    public API = 'http://localhost:8080';
 

    constructor(public http: HttpClient){
    }

    getProprietaire(id: number) : Observable<Proprietaire> {
        return this.http.get<Proprietaire>(this.API + '/proprietaire/' + id);
        
      }

      getProprietaireByIdentifiant(identifiant: string) : Observable<Proprietaire> {
        return this.http.get<Proprietaire>(this.API + '/proprietaires/' + identifiant);
        
      }

      getIdProprietaireByIdentifiant(identifiant: string) : Observable<number> {
        return this.http.get<number>(this.API + '/proprietaireid/' + identifiant);
        
      }

      creationProprietaire(proprietaire : Proprietaire) : Observable<Proprietaire>{
        return this.http.post<Proprietaire>(this.API + '/proprietaire', proprietaire);
    }

    
    isProprietaire(valeur){
      if(valeur == true){
        this.isProp = true;
        console.log(valeur);
        console.log(this.isProp);
        return true;
      }else{
        this.isProp = false;
        console.log(this.isProp);
        return false;
      }
    }

}