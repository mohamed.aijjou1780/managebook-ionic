import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Emprunte } from '../models/emprunte';
import { Emprunteur } from '../models/emprunteur';

@Injectable()
export class EmprunteurService{

    public API = 'http://localhost:8080';



    constructor(public http: HttpClient){
    }

    
    emprunteuracif(id :number, id2 : number) : Observable<Emprunteur>{
    
        return this.http.get<Emprunteur>(this.API + '/proprietaire/' + id + '/emprunteuractif/' + id2 );
    }

    creationEmprunteur(id : number, data: any):Observable<any>{

        return this.http.post<any>(this.API + '/proprietaire/' + id + '/creationemprunteur', data);

    }
    
   
}