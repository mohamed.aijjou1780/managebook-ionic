import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { AlertController, NavController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  constructor(private authService: AuthService, private router: Router, 
    private alertController : AlertController, private navCtrl : NavController) {}

  
  /* canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    const url: string = state.url;
  
    return this.checkLogin(url);
  }
 */



canActivate(): Observable<boolean> | Promise<boolean> | boolean {

  return new Promise(
    (resolve, reject) => {
      if(this.authService.isLoggedIn == true) {
            resolve(true);
          } else {
            this.presentAlert();
           
            resolve(false);
          }
        }
      );
    }
  
    async presentAlert() {
      const alert = await this.alertController.create({
        header: 'Alert',
        subHeader: 'Opération Impossible',
        message: 'Retour à la page Accueil',
        buttons: [
          {
            text: 'OK',
            role: 'OK',
            cssClass: 'secondary',
            handler: () =>{
    
              this.router.navigate(['home']);
    
          }
    
        }
        ], 
       
      });
    
      await alert.present();
    }
  
 /*  
  checkLogin(url: string): boolean {
    console.log(this.authService.isLoggedIn)
    if (this.authService.islogin()) {   
      console.log('c est autorise');
      return true; }
     this.authService.redirectUrl = url; 
     this.router.navigate(['login']);
    return false;
  } 
  */
  /* canActivate(route: ActivatedRouteSnapshot): boolean {
    console.log(route);

    let authInfo = {
      authenticated: false
    };

    if (!authInfo.authenticated) {
      this.router.navigate(['login']);
      return false;
    }

    return true;
  } */
  /* canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    return new Promise((resolve, reject) => {
      storage.ready().then(() => {
        storage.get('currentUser').then((val) => {
          if (val) {
            resolve(true);
          } else {
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
            resolve(false);
          }
        });
      });
    });
  } */
}
