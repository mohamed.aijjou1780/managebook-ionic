import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, NavController, NavParams } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { ProduitsComponent } from './pages/produits/produits.component';
import { ProduitService } from './services/service-produit';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ProprietaireUniqueComponent } from './pages/proprietaire-unique/proprietaire-unique.component';
import { ProprietaireService } from './services/service-proprietaire';
import { ListeProduitProprietaireComponent } from './pages/liste-produit-proprietaire/liste-produit-proprietaire.component';
import { DatePipe } from '@angular/common';
import { EmprunteurService } from './services/service-emprunteur';
import { PretComponent } from './pages/pret/pret.component';
import { Validators, FormBuilder, FormGroup, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CreationProduitComponent } from './pages/creation-produit/creation-produit.component';
import { CommentaireComponent } from './pages/commentaire/commentaire.component';
import { SearchPageComponent } from './pages/search-page/search-page.component';
import { SearchBarcodeComponent } from './pages/search-barcode/search-barcode.component';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { TokenInterceptor } from './interceptors/token.interceptors';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { AuthGuard } from './auth/auth.guard';
import { ParamPrincipalComponent } from './pages/param-principal/param-principal.component';
import { ParamSecondaireComponent } from './pages/param-secondaire/param-secondaire.component';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { CreationEmprunteurComponent } from './pages/creation-emprunteur/creation-emprunteur.component';


@NgModule({
  declarations: [AppComponent,  ProduitsComponent, ProprietaireUniqueComponent, ListeProduitProprietaireComponent,
    PretComponent,  CreationProduitComponent, CommentaireComponent, SearchPageComponent,
    SearchBarcodeComponent, LoginComponent, RegisterComponent, ParamPrincipalComponent, ParamSecondaireComponent, CreationEmprunteurComponent],
  entryComponents: [ ProduitsComponent, ProprietaireUniqueComponent, ListeProduitProprietaireComponent,
    PretComponent, CreationProduitComponent, CommentaireComponent,
    SearchPageComponent, SearchBarcodeComponent, LoginComponent, RegisterComponent, ParamPrincipalComponent, ParamSecondaireComponent, CreationEmprunteurComponent],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, ReactiveFormsModule, HttpClientModule, Ng2GoogleChartsModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    AuthGuard,
    AuthService,
    BarcodeScanner,
    EmprunteurService,
    DatePipe,
    NavParams,
    NavController,
    ProduitService,
    ProprietaireService,
    HttpClientModule,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
