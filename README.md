## ![](logo.png)

![npm version](https://img.shields.io/npm/v/ionic)
![NPM Licence](https://img.shields.io/npm/l/ionic)
![npm bundle size](https://img.shields.io/bundlephobia/min/ionic)

Cette application Front a été developpé avec [Ionic](https://ionicframework.com). Elle doit être associée avec la partie Back-end developpée avec [Spring-boot](https://gitlab.com/mohamed.aijjou1780/managebook). Son objectif est de vous permettre de suivre le prêt de vos objets favoris (📚 💿 🎮 ) et de savoir qui 🤷‍♂️ les detient et depuis quand 📅 ....


## Description 

![](https://forthebadge.com/images/badges/made-with-typescript.svg)
![Demo video](video-managebook.mov)

## Getting Started

### Installing 

##### 1 - Front Part :

- Step 1 : [Installer Node Js](https://nodejs.org/en/)
- Step 2 : Installer le CLI-Ionic : ```npm install -g ionic```
- Step 3 : ``` git clone https://gitlab.com/mohamed.aijjou1780/managebook-ionic.git ```
- Step 4 : ``` npm install ```
- Step 5 : ``` npm start  ou  ng serve ```
- Step 6 : 🥳

##### 2 - Back Part (Spring-boot - PostgreSql):

- Step 1 : [Installer un JDK](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
- Step 2 : [Installer Maven](https://maven.apache.org)
- Step 3 : ``` git clone https://gitlab.com/mohamed.aijjou1780/managebook.git ```
- Step 4 : Add Dependencies : Spring Web - Spring Security - Spring Data Jpa - FlyWay Migration - H2 Database - HyperSql Database - PostgreSQL Driver.
- Step 5 : Run with your IDE.

### Authors

2020 - now : Aijjou mohamed.


